/**
* Abstract class used to create other  shape objects
* @author Created by Dr. Morshipor, completed by Abdulkareem Dolapo
* @version 1.0
* @since February 3rd, 2018
**/
abstract class Shape implements Comparable<Shape>{
	/**
	* Shape Origin
	**/
	protected Point origin;

	/**
	* Shape name
	**/
	protected Text name;

	/**
  * Abstract methods to calculate perimeter, area, and volume when applicable
	**/
	abstract protected Double area();
	abstract protected Double perimeter();
	abstract protected Double volume();

	/**
	* Constructor
	**/
	protected Shape(Double x_origin, Double y_origin, String name, Colour colour){

		origin = new Point(x_origin,y_origin, colour);
		this.name = new Text(name);
	}

	/**
	* Fetches  the shape Origin
	* @return origin as a point objects
	**/
	protected Point  getOrigin()   {
		return origin;
	}

	/**
	* Fetches name of shapes
	* @return name of shape
	**/
	protected String  getName()   {
		return name.getText();
	}

	/**
	* Returns the distance between this shape and another
	* @return distance
	**/
	protected  Double distance(   Shape  other){
		return origin.distance(other.origin);
	}

	/**
	* Returns the distance between two shapes
	* @param a shape 1
	* @param b shape 2
	* @return distance
	**/
	protected Double  distance(Shape a, Shape  b){
		return Point.distance(a.origin, b.origin);
	}

	/**
	* Moves the origin x and y position by dx and dy respecively
	* @param dx
	* @param dy
	* */
	protected void  move(Double dx, Double dy){
		origin.setx(origin.getx()+dx);
		origin.sety(origin.gety()+dy);
	}

	/**
	* compareTo function that compares this shape to another shape
	* @return -1 if this shape is smaller, 0  if equal, and a positive number if
	* greater
	**/
	@Override
	public int compareTo(Shape otherShape) {
		return(this.getName().compareTo(otherShape.getName()));

	}

	/**
	* Converts shape properties to a printables String
	* @return properties of shapes in a string format
	**/
	@Override
	public String toString(){
		String s = "\nShape name: " + name + "\nOrigin: " + origin;
		return s;
	}

}
