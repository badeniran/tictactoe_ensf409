import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 * A type of player that lets a human determine cells on board to mark
 * @author Abdulkareem Dolapo
 * @version 1.0
 * @since February 6th, 2018
 */
public class HumanPlayer extends Player{
	/**
	 * Human player constructor
	 * @param pName player name
	 * @param pMark player mark
	 */
	public HumanPlayer(String pName, char pMark) {
		super(pName, pMark);
	}
	
	/**
	 * Rotates playing turns among players and determines if the end of the game has been reached
	 */
	public void play() {
		if(!(board.oWins()||board.xWins()||board.isFull())) {
			makeMove();
			board.display();
			opponent.play();
		}
		else if(board.oWins()) {
			if (mark == LETTER_O)
				System.out.println(name + " is the winner of this round of Tic-tac-toe!");
			else
				System.out.println(opponent.getName() + "is the winner of this round of Tic-tac-toe!");			
		}
		else if(board.xWins()) {
			if(mark == LETTER_X)
				System.out.println(name + " is the winner of this round of Tic-tac-toe!");
			else
				System.out.println(opponent.getName() + " is the winner of this round of Tic-tac-toe!");
		}
		else
			System.out.println("This round of Tic-tac-toe ends in a tie");
		
	}
	
	/**
	 * Allows player to mark cells in the board (or fill cells in the board array)
	 */
	public void makeMove() {
		System.out.println("Might "+ name +" please enter row and column numbers (in that order) seperated by a space for next move");
		BufferedReader stdin;
		stdin = new BufferedReader(new InputStreamReader(System.in));
		
		//Scanner scan = new Scanner(System.in);
		//scan.nextLine();

		
		try {
			String temp = stdin.readLine(); //scan.nextLine();
			String[] temp2 = temp.split(" ");
			int row = Integer.parseInt(temp2[0]);
			int column = Integer.parseInt(temp2[1]);
			
			if(board.getMark(row, column) == SPACE_CHAR)
				board.addMark(row, column, mark);
			else {
				System.out.println("That's an illegal move. Try again.");
				this.play();
			}
		}
		catch(Exception e) {
			System.out.println("You have entered invalid cell locations. Game over!");
			System.exit(0);
		}		
	}		
	
}