
/**
 * This class helps to acquaint player objects with their opponents while also
 * initiating the start of the game.
 * @author Abdulkareem Dolapo
 * @version 1.0
 * @since January 31st, 2018
 */
public class Referee implements Constants{
	
	/**
	 * X-Player in current game session
	 */
	private Player xPlayer;
	/**
	 * O-Player in current game session
	 */
	private Player oPlayer;
	/**
	 * Common game board
	 */
	private Board board;
	
	/**
	 * Default Constructor that does nothing
	 */
	public Referee() {
		//Do nothing
	}
	
	/**
	 * Acquaints players with opponents and initiates the start of the game
	 */
	public void runTheGame() {
		xPlayer.setOpponent(oPlayer);
		oPlayer.setOpponent(xPlayer);
		System.out.printf("\n");
		System.out.println( "MAY THE GAME OF TIC TAC and TOE BEGIN!"
							+ " You are RESPONSIBLE for entering"
							+ " correct inputs to valid board cells."
							+ " To play to completion, you shall abide by these rules.");
		board.display();
		xPlayer.play();
	}
	
	/**
	 * Shares the common game board with the referee
	 * @param board Game board
	 */
	public void setBoard(Board board) {
		this.board = board;
	}
	
	/**
	 * Sets referee's O-Player to the O-Player in the current game session
	 * @param oPlayer Current O-Player
	 */
	public void setoPlayer(Player oPlayer) {
		this.oPlayer = oPlayer;
	}
	
	/**
	 * Sets referees X-Player to the X-player in current game session
	 * @param xPlayer current X-player
	 */
	public void setxPlayer(Player xPlayer) {
		this.xPlayer = xPlayer;
	}
	
}