/**
* Contains the data fields and methods to create a Java data type that represents
* a computer controlled player that will purposely try and win and block wins in the game of tic-tac-toe.
* @author Babafemi Adeniran
* @version 1.0
* @since February 4th, 2018
*/

public class SmartPlayer extends BlockingPlayer{

  /**
   * Constructs a smart player and assigns specified name and mark to the object
   * @param name specified player name
   * @param mark assigned mark
   */
  public SmartPlayer(String name, char mark){
    super(name, mark);
  }

  /**
	 * Allows Smart player to mark cells in the board.
   * The player first looks to see if it can win, and in that case it will place a mark to win.
   * Then opponent is about to win, and in that case
   * it will block the opponent from winning. Otherwise, it will place a mark in a random
   * empty space.
	 */
  protected void makeMove(){
    for (int i = 0; i < 3; i++){
      for (int j = 0; j < 3; j++){
        if (board.getMark(i, j) == SPACE_CHAR){
          if (testForWinning(i, j)){
            board.addMark(i, j, mark);
            return;
          }
        }
      }
    }
    super.makeMove();
  }

  /**
  * Checks if the current position can result in a win.
  * @param row the row coordinate to be checked
  * @param col the column coordinate to be checked
  * @return true if the current position will result in a win.
  *         Otherwise false.
  */
  protected boolean testForWinning(int row, int col){
    if (row == 1 || col == 1){
      if (row == col){
        return (testUpToDown(col) || testLeftToRight(row) || testDiag1() || testDiag2());
      }
      else {
        return testUpToDown(col) || testLeftToRight(row);
      }
    }
    else if (row - col == 0){
      return (testUpToDown(col) || testLeftToRight(row) || testDiag1());
    }
    else{
      return (testUpToDown(col) || testLeftToRight(row) || testDiag2());
    }
  }

  /**
  * Tests to see if the blocks if the columns of the current row could result in a win for the player.
  * @param row the row of the coordinate
  * @return true if the spaces to the left and right of  the specified row
  *         could result in a win for the player. Otherwise false.
  */
  private boolean testLeftToRight(int row){
    int count = 0;
    for (int i = 0; i < 3; i++){
      char markerLR = board.getMark(row, i);
      if (markerLR == mark){
        count++;
      }
      if (count == 2){
        return true;
      }
    }
    return false;
  }

  /**
  * Tests to see if the blocks if the rows of the current column could result in a win for the player.
  * @param col the column of the coordinate
  * @return true if the spaces above and below of the specified column
  *         could result in a win for the player. Otherwise false.
  */
  private boolean testUpToDown(int col){
    int count = 0;
    for (int i = 0; i < 3; i++){
      char markerUD = board.getMark(i, col);
      if (markerUD == mark){
        count++;
      }
      if (count == 2){
        return true;
      }
    }
    return false;
  }

  /**
  * Tests to see if the diagonal from top left to bottom right could result in a win for the player.
  * @return true if the diagonal
  *         could result in a win for the player. Otherwise false.
  */
  private boolean testDiag1(){
    int count = 0;
    for (int i = 0; i < 3; i++){
      char markerDiag = board.getMark(i, i);
      if (markerDiag == mark){
        count++;
      }
      if (count == 2){
        return true;
      }
    }
    return false;
  }

  /**
  * Tests to see if the diagonal from top right to bottom left could result in a win for the player.
  * @return true if the diagonal
  *         could result in a win for the player. Otherwise false.
  */
  private boolean testDiag2(){
    int count = 0;
    for (int i = 0; i < 3; i++){
      char markerDiag = board.getMark(3-1-i, i);
      if (markerDiag == mark){
        count++;
      }
      if (count == 2){
        return true;
      }
    }
    return false;
  }
}
