/**
 * A structure that stores a player's name and assigned mark while granting player access to the board
 * in this game of Tic-tac-toe
 * @author Abdulkareem Dolapo
 * @version 2.0
 * @since February 6th, 2018
 */
abstract class Player implements Constants{
	/**
	 * Name of player
	 */
	protected String name;
	/**
	 * Game board reference, permits multiple players to play on the same board
	 */
	protected Board board;
	/**
	 * Second player in game. 
	 */
	protected Player opponent;
	/**
	 * Player mark, could be X or O
	 */
	protected char mark;
	
	abstract protected void makeMove();
	
	abstract protected void play();
	
	/**
	 * Constructs a player and assigns specified name and mark to player object
	 * @param pName specified player name
	 * @param pMark assigned mark
	 */
	public Player(String pName, char pMark) {
		name = pName;
		mark = pMark;
	}
	
	/**
	 * Sets Player opponent with specified Player object
	 * @param pOpponent specified Player object
	 */
	protected void setOpponent(Player pOpponent) {
		opponent = pOpponent;
		
	}
	
	/**
	 * Sets the Game board as common board for players
	 * @param theBoard Game board
	 */
	protected void setBoard(Board theBoard) {
		board = theBoard;
		
	}
	/**
	 * Fetches name of player
	 * @return Player name
	 */
	protected String getName() {
		return name;
	}
}