/**
 * A TicTacToe player that plays autonomously by marking random empty cells on the board
 * @author Abdulkareem Dolapo
 * @version 1.0
 * @since February 6th, 2018
 */
public class RandomPlayer extends Player{
	/**
	 * Random number generator that generates row and column numbers
	 */
	RandomGenerator randGen;
	
	/**
	 * Constructor for RandomPlayer object
	 * @param name player name
	 * @param mark player mark
	 */
	public RandomPlayer(String pName, char pMark) {
		super(pName, pMark);
		randGen = new RandomGenerator();
	}
	
	/**
	 * Rotates playing turns among players and determines if the end of the game has been reached
	 */
	public void play() {
		if(!(board.oWins()||board.xWins()||board.isFull())) {
			makeMove();
			board.display();
			opponent.play();
		}
		else if(board.oWins()) {
			if (mark == LETTER_O)
				System.out.println(name + " is the winner of this round of Tic-tac-toe!");
			else
				System.out.println(opponent.getName() + " is the winner of this round of Tic-tac-toe!");			
		}
		else if(board.xWins()) {
			if(mark == LETTER_X)
				System.out.println(name + " is the winner of this round of Tic-tac-toe!");
			else
				System.out.println(opponent.getName() + " is the winner of this round of Tic-tac-toe!");
		}
		else
			System.out.println("This round of Tic-tac-toe ends in a tie");
		
	}
	
	/**
	 * Randomly decides which empty cell the random player marks
	 */
	protected void makeMove() {
		System.out.println("Hold on while " + name + " makes a move");
		while (true){
			int row = randGen.discrete(0, 2);
			int col = randGen.discrete(0, 2);
			if(board.getMark(row, col) == SPACE_CHAR) {
				board.addMark(row, col, mark);
				break;
			}
		}
	}
}